#include <stdio.h>
#include <iostream>

#include "coreclrhost.h"
#include "DotNetRuntime.h"

int main(int argc, char* argv[])
{
    // Start up CoreCLR via RAII
    Pls::DotNetRuntime dotnet;
    
    // Get function pointers to managed static functions (non-statics are not supported)
    using blankFunc = void(*)(void);
    blankFunc print = dotnet.GetFunctionPtr<blankFunc>
    (
        "ManagedLibrary",
        "ManagedLibrary.ManagedWorker",
        "Print"
    );
    using stringFunc = void(*)(const char*);
    stringFunc printText = dotnet.GetFunctionPtr<stringFunc>
    (
        "ManagedLibrary",
        "ManagedLibrary.ManagedWorker",
        "PrintText"
    );
    using powFunc = double(*)(double, double);
    powFunc power = dotnet.GetFunctionPtr<powFunc>
    (
        "ManagedLibrary",
        "ManagedLibrary.ManagedWorker",
        "Power"
    );
    using arrFunc = double(*)(double*, int);
    arrFunc sum = dotnet.GetFunctionPtr<arrFunc>
    (
        "ManagedLibrary",
        "ManagedLibrary.ManagedWorker",
        "Sum"
    );
    using stringRetFunc = bool(*)(double, char*, int);
    stringRetFunc getStr = dotnet.GetFunctionPtr<stringRetFunc>
    (
        "ManagedLibrary",
        "ManagedLibrary.ManagedWorker",
        "GetString"
    );

    // Invoking managed functions
    print();
    printText("Hello World!");
    printText(std::to_string(power(5.0, 3.0)).c_str());

    // Array
    std::cout << "Array Demo:" << std::endl;
    constexpr int COUNT = 5;
    double arr[COUNT] = { 1.0, 2.0, 3.0, 4.0, 10.0 };
    printText(std::to_string(sum(arr, COUNT)).c_str());

    // Return string
    char buffer[256] = "Hello";
    getStr(12.23, buffer, 256);
    std::cout << "String Return Demo: " << buffer << std::endl;

    return 0;
}