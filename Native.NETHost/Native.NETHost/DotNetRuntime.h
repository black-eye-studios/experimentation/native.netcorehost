#pragma once

#include <iomanip>          // std::setfill, std::setw
#include <stdexcept>        // std::runtime_error
#include <string>           // std::string
#include <sstream>          // std::ostringstream
#include <Windows.h>        // HMODULE

#include "coreclrhost.h"    // coreclr_*
#include "Exceptions.h"     // FunctionPtrNotFoundException

namespace Pls
{
    /********************************************************************************//*!
    @brief    Class that encapsulates the state of the NET Core Runtime lifecycle.
    *//*********************************************************************************/
    class DotNetRuntime
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructor                                                     */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Initializes the DotNetRuntime. Loads the CoreCLR and grabs pointers to
                  bootstrapping functions and kickstarts the CoreCLR.
        *//*****************************************************************************/
        DotNetRuntime();
        /****************************************************************************//*!
        @brief    Unloads the CoreCLR.
        *//*****************************************************************************/
        ~DotNetRuntime();

        // Disallow copy and moving
        DotNetRuntime(const DotNetRuntime&) = delete;
        DotNetRuntime(DotNetRuntime&&) = delete;

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves a function pointer from the a CLR assembly based on the 
                  specified assembly, type and function names.

        @tparam        FunctionType
                Type of the function pointer that the specified function name will 
                provide.
                
        @params[in]    assemblyName
                Name of the CoreCLR assembly that contains the function.
        @params[in]    typeName
                Name of the CoreCLR type in the assembly that contains the function.
                Nested types are separated by a period(.).
        @params[in]    functionName
                Name of the CoreCLR function to get a pointer to.

        @returns  Pointer to the function in the assembly that was specified.
        *//*****************************************************************************/
        template<typename FunctionType>
        FunctionType GetFunctionPtr(const std::string& assemblyName, 
                                    const std::string& typeName, 
                                    const std::string& functionName);

      private:
        /*-----------------------------------------------------------------------------*/
        /* Data Members                                                                */
        /*-----------------------------------------------------------------------------*/
        // References to CoreCLR key components
        HMODULE coreClr = nullptr;
        void* hostHandle = nullptr;
        unsigned int domainId = 0;
        // Function Pointers to CoreCLR functions
        coreclr_initialize_ptr      initializeCoreClr     = nullptr;
        coreclr_create_delegate_ptr createManagedDelegate = nullptr;
        coreclr_shutdown_ptr        shutdownCoreClr       = nullptr;

        /*-----------------------------------------------------------------------------*/
        /* Helper Functions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves a function pointer from the CoreCLR based on the specified
                  function name.

        @tparam        FunctionType
                Type of the function pointer that the specified function name will 
                provide.

        @params[in]    functionName
                Name of the CoreCLR function to get a pointer to.

        @returns  Pointer to the function in the CoreCLR that was specified.
        *//*****************************************************************************/
        template<typename FunctionType>
        FunctionType getCoreClrFunctionPtr(const std::string& functionName);
        /****************************************************************************//*!
        @brief    Compiles a semicolon separated string of trusted platform assemblies by
                  searching the specified directory.

        @params[in]    directory
                Path to the directory where the trusted platform assemblies reside.

        @returns  Semicolon separated string of trusted platform assemblies.
        *//*****************************************************************************/
        static std::string buildTpaList(const std::string& directory);
    };

    /*---------------------------------------------------------------------------------*/
    /* Template Function Implementations                                               */
    /*---------------------------------------------------------------------------------*/
    template<typename FunctionType>
    FunctionType DotNetRuntime::GetFunctionPtr(const std::string & assemblyName, 
                                               const std::string & typeName, 
                                               const std::string & functionName)
    {
        FunctionType managedDelegate = nullptr;    
        int result = createManagedDelegate
        (
            hostHandle, 
            domainId,
            assemblyName.c_str(),
            typeName.c_str(),
            functionName.c_str(),
            (void**)&managedDelegate
        );

        // Check if it failed
        if (result < 0)
        {
            
            std::ostringstream oss;
            oss << std::hex << std::setfill('0') << std::setw(8) 
                << "[DotNetRuntime] Failed to get pointer to function \"" 
                << typeName << "." << functionName << "\" in assembly (" << assemblyName << "). "  
                << "Error 0x" << result << "\n";
            throw FunctionPtrNotFoundException(oss.str());
        }

        return managedDelegate;
    }
    template<typename FunctionType>
    FunctionType DotNetRuntime::getCoreClrFunctionPtr(const std::string& functionName)
    {
        FunctionType fPtr = reinterpret_cast<FunctionType>(GetProcAddress(coreClr, functionName.c_str()));
        if (!fPtr)
        {
            std::ostringstream oss;
            oss << "[DotNetRuntime] Unable to get pointer to function: \"" << functionName << "\"";
            throw FunctionPtrNotFoundException(oss.str());
        }
        return fPtr;
    }
}

