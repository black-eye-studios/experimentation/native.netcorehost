#include "DotNetRuntime.h"

#include <shlwapi.h> // PathRemoveFileSpecA
#include <iostream>  // std::cerr

namespace Pls
{
    /*---------------------------------------------------------------------------------*/
    /* Constructors/Destructor                                                         */
    /*---------------------------------------------------------------------------------*/
    DotNetRuntime::DotNetRuntime()
    {
        // Get the current executable directory
        std::string runtimePath(MAX_PATH, '\0');
        GetModuleFileNameA(nullptr, runtimePath.data(), MAX_PATH);
        PathRemoveFileSpecA(runtimePath.data()); 
        // Since PathRemoveFileSpecA() removes from data(), the size is not updated, so we must manually update it
        runtimePath.resize(std::strlen(runtimePath.data())); 

        // Construct the CoreCLR path
        std::string coreClrPath(runtimePath); // Works
        coreClrPath += "\\coreclr.dll";
    
        // Load the CoreCLR DLL
        coreClr = LoadLibraryExA(coreClrPath.c_str(), nullptr, 0);
        if (!coreClr)
        {
            std::ostringstream oss;
            oss << "[DotNetRuntime] Error #" << GetLastError() << " Failed to load CoreCLR from \"" << coreClrPath << "\"\n";
            throw SystemInitException(oss.str());
        }

        // Step 2: Get CoreCLR hosting functions
        initializeCoreClr     = getCoreClrFunctionPtr<coreclr_initialize_ptr>("coreclr_initialize");
        createManagedDelegate = getCoreClrFunctionPtr<coreclr_create_delegate_ptr>("coreclr_create_delegate");
        shutdownCoreClr       = getCoreClrFunctionPtr<coreclr_shutdown_ptr>("coreclr_shutdown");

        // Step 3: Construct AppDomain properties used when starting the runtime
        // Construct the trusted platform assemblies (TPA) list
        // This is the list of assemblies that .NET Core can load as
        // trusted system assemblies (similar to the .NET Framework GAC).
        // For this host (as with most), assemblies next to CoreCLR will 
        // be included in the TPA list
        std::string tpaList = buildTpaList(runtimePath);

        // Define CoreCLR properties
        const char* propertyKeys[] = 
        {
            "TRUSTED_PLATFORM_ASSEMBLIES",      // Trusted assemblies (like the GAC)
            "APP_PATHS",                        // Directories to probe for application assemblies
            // "APP_NI_PATHS",                     // Directories to probe for application native images (not used in this sample)
            // "NATIVE_DLL_SEARCH_DIRECTORIES",    // Directories to probe for native dlls (not used in this sample)
        };
        const char* propertyValues[] = 
        {
            tpaList.c_str(),
            runtimePath.c_str()
        };

        // Step 4: Start the CoreCLR runtime
        int result = initializeCoreClr
        (
            runtimePath.c_str(),                   // AppDomain base path
            "SampleHost",                          // AppDomain friendly name
            sizeof(propertyKeys) / sizeof(char*),  // Property count
            propertyKeys,                          // Property names
            propertyValues,                        // Property values
            &hostHandle,                           // Host handle
            &domainId                              // AppDomain ID
        );

        // Check if intiialization of CoreCLR failed
        if (result < 0)
        {
            std::ostringstream oss;
            oss << std::hex << std::setfill('0') << std::setw(8) 
                << "[DotNetRuntime] Failed to initialize CoreCLR. Error 0x" << result << "\n";
            throw SystemInitException(oss.str());
        }            
    }

    DotNetRuntime::~DotNetRuntime()
    {
        // NOTE: Destructors are noexcept, so we output to cerr instead.
        
        // Shutdown CoreCLR
        int result = shutdownCoreClr(hostHandle, domainId);
        if (result < 0)
        {
            std::cerr << std::hex << std::setfill('0') << std::setw(8) 
                      << "[DotNetRuntime] Failed to shut down CoreCLR. Error 0x" << result << "\n";
        }

        // Unload the DLL
        if (!FreeLibrary(coreClr))
        {
            std::cerr  << "[DotNetRuntime] Failed to free \"coreclr.dll\".\n";
        }
    }
    
    /*---------------------------------------------------------------------------------*/
    /* Helper Functions                                                                */
    /*---------------------------------------------------------------------------------*/
    std::string DotNetRuntime::buildTpaList(const std::string& directory)
    {
        // Constants
        static const std::string SEARCH_PATH = directory + "\\*.dll";
        static constexpr char PATH_DELIMITER = ';';

        // Create a osstream object to compile the string
        std::ostringstream tpaList;

        // Search the current directory for the TPAs (.DLLs)
        WIN32_FIND_DATAA findData;
        HANDLE fileHandle = FindFirstFileA(SEARCH_PATH.c_str(), &findData);
        if (fileHandle != INVALID_HANDLE_VALUE)
        {
            do
            {
                // Append the assembly to the list
                tpaList << directory << '\\' << findData.cFileName << PATH_DELIMITER;

                // Note that the CLR does not guarantee which assembly will be loaded if an assembly
                // is in the TPA list multiple times (perhaps from different paths or perhaps with different NI/NI.dll
                // extensions. Therefore, a real host should probably add items to the list in priority order and only
                // add a file if it's not already present on the list.
                //
                // For this simple sample, though, and because we're only loading TPA assemblies from a single path,
                // and have no native images, we can ignore that complication.
            }
            while (FindNextFileA(fileHandle, &findData));
            FindClose(fileHandle);
        }

        return tpaList.str();
    }
}
