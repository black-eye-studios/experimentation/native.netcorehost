﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace ManagedLibrary
{
    public class ManagedWorker
    {
        public static void Print()
        {
            Console.WriteLine("ManagedWorker.StaticPrint()");
        }

        public static void PrintText(string text)
        {
            Console.WriteLine($"ManagedWorker.StaticPrint({text})");
        }

        public static double Power(double baseValue, double exponent)
        {
            return Math.Pow(baseValue, exponent);
        }

        public static double Sum(IntPtr arrPtr, int count)
        {
            double[] arr = new double[count];
            Marshal.Copy(arrPtr, arr, 0, count);

            double d = 0.0;
            for (int i = 0; i < count; ++i)
            {
                d += arr[i];
            }
            return d;

        }

        public static bool GetString(double value, StringBuilder text, int size)
        {
            _ = text.Clear().Append(value.ToString());
            return true;
        }
    }
}
